﻿using static System.Console;
using System;
using System.Threading;

namespace Aniamls
{
    class Mammal : Animal
    {
        public int PupCount { set; get; }

        public string Species
        {
            set { this.species = value; }
            get { return this.species; }
        }

        public Mammal()
        {
            this.lifetime = 0;
            this.pupsPerYear = 0;
            this.isWild = false;
            this.sex = "";
            this.species = "";
        }

        public virtual void Feed()
        {
            if (this.Species != "" && this.PupCount != 0)
            {
                int HungryOnes = new Random().Next(0, this.PupCount);
                for (int i = 0; i < HungryOnes; i++)
                {
                    Write("\nFeeding");
                    for (int dot = 0; dot <= 10; dot++)
                    {
                        Thread.Sleep(200);
                        Write(".");
                    }
                }
                if (HungryOnes < this.PupCount && HungryOnes != 0)
                    WriteLine("\nOthers are not hungry...");
                else if (HungryOnes == 0)
                    WriteLine("None of them was hungry.");
                else
                    WriteLine("\nAll of Them are fed.");
            }
            else if (this.Species == "")
            {
                WriteLine("I do not know whether this Mammal can have pups or no as the type of species is not clear to me. Sorry, I Cannot Feed them.");
            }
            else
                WriteLine("This {0} has no pups.", this.Species);
        }
    }
}
