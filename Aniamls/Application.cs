﻿using static System.Console;
using System.Threading;

namespace Aniamls
{
    class Application
    {
        static void Main()
        {
            WriteLine("Code Compiled!\n");
            UseMammalClass();
            UseBirdClass();
            UseWaterAnimalClass();
            UsePenguinClass();
            ReadKey();
        }

        public static void UseMammalClass()
        {   
            Mammal tiger = new Mammal
            {
                Species = "Tiger"
            };
            Write("How many pups does this tiger have ?\nSet: ");
            tiger.PupCount = System.Int32.Parse(ReadLine());
            WriteLine("Do You want me to feed them ? (y / n)");
            if (ReadLine() == "y")
                tiger.Feed();
            else
                WriteLine("Ok, I also think they are not hungry.");
        }

        public static void UseBirdClass()
        {
            Bird bird = new Bird();
            WriteLine("\nThis bird is a Parrot.");
            bird.Species = "Parrot";
            bird.AverageFlyDistance = 4;
            bird.Migrates = true;
            WriteLine("oops!");
            bird.TellMeInfo();
        }

        public static void UseWaterAnimalClass()
        {
            WaterAnimal waterAnimal = new WaterAnimal();
            WriteLine("\nSay the name of a Water Animal you want to get.");
            waterAnimal.Species = ReadLine();
            WriteLine("Wait for seconds.");
            for (int dot = 0; dot <= 10; dot++)
            {
                Thread.Sleep(200);
                Write(".");
            }
            Write("\n");
            waterAnimal.GetWaterInfo();
            WriteLine("Ok. I'll find something for you.");
        }

        public static void UsePenguinClass()
        {
            Penguin EmperrorPenguin = new Penguin
            {
                Species = "Emperor Penguin"
            };

            WriteLine("\nGetting Info about {0}", EmperrorPenguin.Species);
            WriteLine("Wait a second.");
            for (int dot = 0; dot <= 10; dot++)
            {
                Thread.Sleep(200);
                Write(".");
            }
            Write("\n");
            EmperrorPenguin.TellMeInfo();
        }
    }
}