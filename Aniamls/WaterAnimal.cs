﻿using static System.Console;
using System;

namespace Aniamls
{
    class WaterAnimal : Animal, IWater
    {
        public WaterTypes WaterType { get; set; }

        public string Species
        {
            set
            {
                this.species = value;
                if (this.WaterType == WaterTypes.Unset)
                {
                    WriteLine("Now You must set the type of water where the {0} live.", this.Species);
                    WriteLine("1: Salty\n2: Sweet\n3: Aquarium\n4: WaterSurface\n");
                    do
                    {
                        Write("Water Type #: ");
                        Int32 WaterValue = Int32.Parse(ReadLine());
                        if (WaterValue > 0 && WaterValue <= 4)
                            this.WaterType = (WaterTypes)WaterValue;
                        else
                            WriteLine("Incorrect Value: Try again.");
                    } while (this.WaterType == WaterTypes.Unset);
                }
            }
            get { return this.species; }
        }

        public virtual void GetWaterInfo()
        {
            if (this.species != "" && this.WaterType != WaterTypes.Aquarium)
                WriteLine("{0} water is a suitable type of water for {1} to live.", this.WaterType, this.Species);
            else if (this.species != "" && this.WaterType == WaterTypes.Aquarium)
                WriteLine("Not any Water Animal can live in an aquarium.");
        }

        public WaterAnimal()
        {
            this.WaterType = WaterTypes.Unset;
            this.lifetime = 0;
            this.pupsPerYear = 0;
            this.isWild = true;
            this.sex = "";
            this.species = "";
        }
    }
}
