﻿using static System.Console;

namespace Aniamls
{
    class Penguin : Bird
    {
        private Egg egg;

        public Penguin()
        {
            this.averageFlyDistance = 0;
            this.species = "Penguin";
            this.lifetime = 20;
            this.isWild = true;
            this.sex = "";
            this.pupsPerYear = 2;
            this.egg = new Egg();
            this.egg.Color = "White-Black dotted";
            this.egg.Weight = 315;
        }

        public new string Species {
            get { return this.species; }
            set { this.species = value; }
        }

        public override void TellMeInfo()
        {
            this.AverageFlyDistanceInfo();
            this.MigrateLocationInfo();
            this.AdvancedInfo();
            WriteLine("Their Eggs have {0} color and weight around {1} grams", this.egg.Color, this.egg.Weight);
        }

        public override void AverageFlyDistanceInfo()
        {
            WriteLine("The {0}s are not Flying.", this.Species);
        }

        public override void MigrateLocationInfo()
        {
            WriteLine("The {0} are not migrating as they live in extremely cold weather in Antarctica.", this.Species);
        }

        public void AdvancedInfo()
        {
            WriteLine("The {0} are wild birds and can live up to {1} years. They bear {2} chicks per year.", this.Species, this.lifetime, this.pupsPerYear);
        }
    }
}