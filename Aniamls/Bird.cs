﻿using static System.Console;

namespace Aniamls
{
    class Bird : Animal, IFly
    {
        private bool migrates;
        protected float averageFlyDistance;

        public float AverageFlyDistance
        {
            set
            {
                if (value >= 0 && this.Species != "")
                    this.averageFlyDistance = value;
                else if (this.Species == "")
                    WriteLine("You must set the bird species first.");
                else
                    WriteLine("The average flight distance of a bird cannot be negative.");
            }
            get { return this.averageFlyDistance; }
        }

        public string Species
        {
            set { this.species = value; }
            get { return this.species; }
        }

        public bool Migrates
        {   
            set
            {
                if (this.Species != "")
                    this.migrates = value;
                else if (this.Species == "")
                    WriteLine("You must set the bird species first to be able to set the migration info.");
            }
            get { return this.migrates; }
        }

        public Bird(int lifeTime = 0, int pupsPerYear = 0, bool wild = false, string sex = "",
            string species = "", bool slept = false)
        {
            this.lifetime = lifeTime;
            this.pupsPerYear = pupsPerYear;
            this.isWild = wild;
            this.sex = sex;
            this.species = species;
            this.slept = slept;
            this.wakeUptime = new System.DateTime(0);
            this.supply = new Food[1];
            this.supply[0] = new Food("Water", 1);
        }

        public virtual void TellMeInfo()
        {
            this.MigrateLocationInfo();
            this.AverageFlyDistanceInfo();
        }
            
        //Interface Methods
        public virtual void AverageFlyDistanceInfo()
        {
            if (this.Species == "")
                WriteLine("The type of species is not set. We cannot say whether this Bird can definitely fly or no.");
            else
                WriteLine("Did you know that {0}s have an Average Flight Distance of {1} Kilometers?", this.Species, this.AverageFlyDistance);
        }

        public virtual void MigrateLocationInfo()
        {
            if (this.Migrates == true)
                WriteLine("The {0}s are migrating species.", this.Species);
            else if (this.Species != "" && this.Migrates == false)
                WriteLine("The {0}s are not migrating species.", this.Species);
            else if (this.Species == "")
                WriteLine("The type of species is not set. We cannot say whether this Bird can migrate or no.");
        }
    }
}