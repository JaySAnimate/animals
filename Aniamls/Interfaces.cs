﻿namespace Aniamls
{
    enum WaterTypes
    {
        Unset,
        Salty,
        Sweet,
        Aquarium,
        WaterSurface
    }

    interface IFly
    {
        void AverageFlyDistanceInfo();
        void MigrateLocationInfo();
    }

    interface IWater
    {
        void GetWaterInfo();
    }

    interface ILive
    {
        void WakeUp();
        float Eat();
        void Sleep();
    }
}
