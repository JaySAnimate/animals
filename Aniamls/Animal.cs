﻿using System;

namespace Aniamls
{
    struct Food
    {
        public string type;
        public int units;

        public Food(string type, int units)
        {
            this.type = type;
            this.units = units;
        }
    }

    class Animal : ILive
    {
        protected int lifetime;
        protected int pupsPerYear;
        protected bool isWild;
        protected string sex;
        protected string species;
        protected DateTime wakeUptime;
        protected Food[] supply;
        protected bool slept;

        public Animal(int lifeTime = 0, int pupsPerYear = 0, bool wild = false, string sex = "",
            string species = "", bool slept = false)
        {
            this.lifetime = lifeTime;
            this.pupsPerYear = pupsPerYear;
            this.isWild = wild;
            this.sex = sex;
            this.species = species;
            this.slept = slept;
            this.wakeUptime = new DateTime(0);
            this.supply = new Food[1];
            this.supply[0] = new Food("Water", 1);
        }

        public DateTime WakeUpTime {
            get
            {
                return this.wakeUptime;
            }
            set
            {
                this.wakeUptime = value;
            }
                
        }

        public Food[] Supply
        {
            get
            {
                return this.supply;
            }
            set
            {
                this.supply = value;
            }
        }

        public virtual void Sleep() { this.slept = true; }

        public virtual void WakeUp() { this.slept = false; }

        public virtual float Eat()
        {
            //if an animal eats, its supply is decremented
            int remaining = 0;
            for(int i = 0; i < this.supply.Length; i++)
            {
                if(this.supply[i].units != 0)
                    this.supply[i].units--;
            }
            foreach(var sup in this.supply)
                remaining += sup.units;
            return remaining;
        }
    }
}