﻿namespace Aniamls
{
    struct Egg
    {
        public float Weight { set; get; }
        public string Color { set; get; }
    }
}
